<?php

namespace SteeveDroz\CiInstaller;

class Installer {
    private $root;
    private $source;
    private $fcpath;

    public function __construct($root, $source, $fcpath)
    {
        $this->root = $root;
        $this->source = $source;
        $this->fcpath = $fcpath;
    }

    public function install($verbose = false)
    {
        $this->_copy($this->source, './', $verbose);
    }

    private function _copy($source, $path, $verbose)
    {
        if (is_dir($this->root . $path . $source))
        {
            if (!file_exists($this->fcpath . $path . $source))
            {
                mkdir($this->fcpath . $path . $source);
                if ($verbose)
                {
                    echo 'Create directory ' . $this->fcpath . $path . $source . PHP_EOL;
                }
            }

            if ($dir = @opendir($this->root . $path . $source))
            {
                $files = [];
                while (($file = readdir($dir)) !== false)
                {
                    if ($file != '.' && $file != '..')
                    $files[] = $file;
                }
                closedir($dir);

                foreach ($files as $file)
                {
                    $this->_copy($file, $path . $source . '/', $verbose);
                }
            }
        }
        else if (!file_exists($this->fcpath . $path . $source))
        {
            copy($this->root . $path . $source, $this->fcpath . $path . $source);
            if ($verbose)
            {
                echo 'Create file ' . $this->fcpath . $path . $source . PHP_EOL;
            }
        }
        else if ($verbose)
        {
            echo 'File ' . $this->fcpath . $path . $source . ' already existing, nothing happens' . PHP_EOL;
        }
    }
}
